import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './componenets/home/home.component';
import { BooksComponent } from './componenets/books/books.component';
import { NgModule } from '@angular/core';

const routes:Routes = [
    {
        path:'',
        component:HomeComponent
    },
    {
        path:'books',
        component:BooksComponent
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: false })],
    exports: [RouterModule]
})
export class AppRoutingModule { }