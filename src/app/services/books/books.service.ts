import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {mergeMap, map} from 'rxjs/operators';
import { Author } from 'src/app/models/author.model';
import { Book } from 'src/app/models/book.model';

@Injectable()
export class BooksService {

  private booksEndpoint:string = `${environment.apiBase}/books`;
  private authorsEndpoint:string = `${environment.apiBase}/authors`;

  constructor(private http: HttpClient) { }

  private getBooks(){
    return this.http.get(this.booksEndpoint);
  }

  private getAuthors(){
    return this.http.get(this.authorsEndpoint);
  }

  public getBooksData(){
    return this.getBooks().pipe(mergeMap((books:any[]) =>{
      return this.getAuthors().pipe(map((authors:any[])=>{
        let results:Book[] = [];
        results = books.map((book:any) =>{
          let mappedBook = new Book()
          mappedBook.id = book.id;
          mappedBook.publishedOn = new Date(book.publishedOn);
          mappedBook.title = book.title;
          mappedBook.author = authors.find((author:any) => author.id === book.authorId)
          return mappedBook;
        })
        return results;
      }))
    }))
  }
}
