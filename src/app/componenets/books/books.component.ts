import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { BooksService } from 'src/app/services/books/books.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  public books:Book[]
  public isLoading:boolean;
  public hasError:boolean;
  @ViewChild('table') tableBody:ElementRef;
  constructor(private booksService:BooksService) { }

  ngOnInit() {
    this.isLoading = true;
    this.booksService.getBooksData()
    .pipe(finalize(() => this.isLoading = false))
    .subscribe(data =>{
      this.books = data;
      this.hasError = false;
    },error =>{
      console.log(error);
      this.hasError = true;

    })
  }

  changeSecondRow(){
    const row = this.tableBody.nativeElement.querySelector('tr:nth-child(2)');
    if(row){
      row.style.backgroundColor = 'red';
    }
  }

}
