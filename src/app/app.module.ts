import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './componenets/home/home.component';
import { BooksComponent } from './componenets/books/books.component';
import { BooksService } from './services/books/books.service';
import { AppRoutingModule } from './app.routing.module';
import { RouterModule } from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BooksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    
  ],
  providers: [BooksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
