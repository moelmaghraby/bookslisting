import { Author } from "./author.model";

export class Book{
    id:number;
    title:string;
    publishedOn:Date;
    author:Author;

}